#!/usr/bin/env bash

set -xe

directories="
    .local/vim/swap
    .local/vim/backup
    .local/share
    .config
"

symlinks="
    .config/mc
    .gitconfig
    .gitignore
    .gnupg
    .bashrc
    .bash_logout
    .profile
    .local/share/gnote
    .screenrc
    .ssh
    .tmux.conf
    .vim
    .vimrc
    .vimrc.basic
"

cd `dirname $0`
local_dir=`pwd`

for name in $directories
do
    mkdir -p ~/$name
done

for name in $symlinks
do
    if [ -e $local_dir/$name ]
    then
        ln -fTs $local_dir/$name ~/$name
    fi
done

echo DONE
