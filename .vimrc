execute pathogen#infect()

let g:netrw_home = expand('$HOME/.local/vim')

set backupdir=$HOME/.local/vim/backup//,.
set directory=$HOME/.local/vim/swap//,.

set t_Co=256
colorscheme lukas
syntax on

source $HOME/.vimrc.basic

fun! CustomizeForTagLib()
    if &ft =~ 'c\|cpp'
        setlocal tabstop=2 shiftwidth=2 softtabstop=2
    endif
endfun

autocmd BufNewFile,BufRead $HOME/code/taglib/* call CustomizeForTagLib()
