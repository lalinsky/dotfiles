" Vim color file
"
" Author: Lukas Lalinsky <lalinsky@gmail.com>

hi clear

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
        syntax reset
    endif
endif
let g:colors_name="lukas"

"
" Support for 256-color terminal
"
if &t_Co > 255
   hi Boolean         ctermfg=135
   hi Character       ctermfg=144
   hi Number          ctermfg=203
   hi Float           ctermfg=135

   " Strings
   hi String                    ctermfg=10
   hi pythonRawString           ctermfg=156
   hi pythonStrFormatting       ctermfg=193     ctermbg=none

   hi Constant        ctermfg=135               cterm=bold
   hi Cursor          ctermfg=16  ctermbg=253
   hi Debug           ctermfg=225               cterm=bold
   hi Define          ctermfg=81
   hi Delimiter       ctermfg=241

   hi diffAdd         ctermfg=15   ctermbg=24
   hi diffChange      ctermfg=15 ctermbg=239
   hi diffDelete      ctermfg=162 ctermbg=53
   hi diffText                    ctermbg=102 cterm=bold

   hi Directory       ctermfg=118               cterm=bold

   hi Error           ctermfg=15 ctermbg=88

   hi ErrorMsg        ctermfg=199 ctermbg=16    cterm=bold
   hi FoldColumn      ctermfg=67  ctermbg=16
   hi Folded          ctermfg=67  ctermbg=16

   hi Function        ctermfg=226               cterm=none " cterm=underline
   hi link perlFunctionName Function

   hi Identifier      ctermfg=75                cterm=none
   hi phpVarSelector  ctermfg=75				cterm=none


   hi Ignore          ctermfg=244 ctermbg=232
   hi IncSearch       ctermfg=193 ctermbg=16

   hi Keyword         ctermfg=161               cterm=bold
   hi Label           ctermfg=229               cterm=none
   hi Macro           ctermfg=193
   hi SpecialKey      ctermfg=81

   hi MatchParen      ctermfg=15  ctermbg=94 cterm=bold
   hi ModeMsg         ctermfg=229
   hi MoreMsg         ctermfg=229

   " complete menu
   hi Pmenu           ctermfg=81  ctermbg=16
   hi PmenuSel                    ctermbg=244
   hi PmenuSbar                   ctermbg=232
   hi PmenuThumb      ctermfg=81

   hi PreCondit       ctermfg=118               cterm=bold
   hi PreProc         ctermfg=118
   hi Question        ctermfg=81
   hi Search          ctermfg=253 ctermbg=66

   " marks column
   hi SignColumn      ctermfg=118 ctermbg=235
   hi SpecialChar     ctermfg=161               cterm=bold
   hi SpecialComment  ctermfg=245               cterm=bold
   hi Special         ctermfg=81  ctermbg=232
   hi SpecialKey      ctermfg=245

   hi Statement       ctermfg=208               cterm=bold
   hi Conditional     ctermfg=208               cterm=bold
   hi Repeat          ctermfg=208               cterm=bold
   hi Exception       ctermfg=208               cterm=bold

   hi perlStatementList ctermfg=15 cterm=none
   hi perlStatementScalar ctermfg=15 cterm=none
   hi perlStatementNumeric ctermfg=15 cterm=none
   hi perlStatementNew ctermfg=15 cterm=none

   hi link pythonPreCondit Include

   hi Operator        ctermfg=202

   hi StatusLine      ctermfg=25 ctermbg=15
   hi StatusLineNC    ctermfg=243 ctermbg=0
   hi StorageClass    ctermfg=208
   hi Structure       ctermfg=81
   hi Tag             ctermfg=161
   hi Title           ctermfg=166
   hi Todo            ctermfg=231 ctermbg=232   cterm=bold

   hi Typedef         ctermfg=81
   hi Type            ctermfg=81                cterm=none
   hi Underlined      ctermfg=244               cterm=underline

   hi VertSplit       ctermfg=244 ctermbg=232   cterm=bold
   hi VisualNOS                   ctermbg=243
   hi Visual                      ctermbg=238
   hi WarningMsg      ctermfg=231 ctermbg=238   cterm=bold
   hi WildMenu        ctermfg=81  ctermbg=16

"   hi Normal          ctermfg=252 ctermbg=233
   hi Normal          ctermfg=15 ctermbg=233

   hi Comment         ctermfg=243
   hi pythonRun       ctermfg=249 ctermbg=238

   hi CursorLine                  ctermbg=234   cterm=none
   hi CursorColumn                ctermbg=234
   hi LineNr          ctermfg=250 ctermbg=234
   hi NonText         ctermfg=250 ctermbg=234

	hi diffAdded ctermfg=46 cterm=bold
	hi diffRemoved ctermfg=196 cterm=bold
	hi diffLine ctermfg=15 ctermbg=89 cterm=none

	hi diffFile ctermfg=196 ctermbg=237
	hi diffNewFile ctermfg=46 ctermbg=237

end
